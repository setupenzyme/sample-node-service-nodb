const express = require('express');
const cors = require('cors');
// get MongoDB driver connection

const PORT = process.env.PORT || 5000;
const app = express();

app.get('/', (req, res) => {
  res.send('Hello World');
});

app.use(cors());
app.use(express.json());
app.use(require('./routes/record'));

app.listen(PORT, () => {
  console.log(`App running on port ${PORT}.`);
});
